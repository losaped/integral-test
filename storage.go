package integral

import (
	"context"
	"errors"
)

// ErrorKeyNotFound returns if storage has'nt this key
var ErrorKeyNotFound = errors.New("value not found")

// Storage interface that allow Get, Set and Delete value by key
type Storage interface {
	Get(string) ([]byte, error)
	Set(string, []byte, context.Context, context.CancelFunc)
	Delete(string)
	Stop()
}
