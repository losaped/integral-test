package integral

type Client interface {
	ReadString(b byte) (string, error)
	ReadBytes(b byte) ([]byte, error)
	Write([]byte) (int, error)
	WriteString(string) (int, error)
}
