package tcpserver

import (
	"context"
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"bitbucket.org/losaped/integral"
	"github.com/sirupsen/logrus"
)

const (
	setCommand = "set"
	getCommand = "get"
	delCommand = "del"
)

type Config struct {
	Port      int
	TTLSecond time.Duration
}

type Server struct {
	*Config
	Storage integral.Storage
}

func (s Server) Run(ctx context.Context) error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", s.Port))
	if err != nil {
		return err
	}

	defer listener.Close()

	logrus.Infoln("listening port:", s.Port)

	go func() {
		for {
			select {
			case <-ctx.Done():
				listener.Close()
				return
			default:
			}
		}
	}()

	for {
		if err := ctx.Err(); err != nil {
			logrus.Info(err)
			return err
		}

		con, err := listener.Accept()
		if err != nil {
			logrus.WithFields(logrus.Fields{"error": err}).Error("on accept connection")
		} else {
			go s.handle(con)
		}

	}
}

func (s *Server) handle(con io.ReadWriteCloser) {
	defer con.Close()

	c := NewClient(con, con)

	for {
		rawCommand, err := c.reader.ReadString('\n')
		if err != nil {
			logrus.WithFields(logrus.Fields{"error": err, "buf": rawCommand}).Error("on reading command")
			c.writer.Write([]byte(err.Error()))
			c.writer.Flush()
			return
		}

		var cmdErr error
		cmd := strings.ToLower(string(rawCommand[:3]))
		logrus.WithFields(logrus.Fields{"cmd": cmd}).Info("command readed")

		switch cmd {
		case setCommand:
			cmdErr = s.setCommand(c)
			break
		case getCommand:
			cmdErr = s.getCommand(c)
			break
		case delCommand:
			cmdErr = s.delCommand(c)
			break
		default:
			c.writer.Write([]byte(integral.ErrorUnknownCommand.Error()))
		}

		if cmdErr != nil {
			c.WriteString(cmdErr.Error() + "\n")
		}
	}

}

func (s *Server) setCommand(c integral.Client) error {
	logrus.Info("entering to set command")

	key, err := c.ReadString('\n')
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Error("on reading key")
		return err
	}

	logrus.WithFields(logrus.Fields{"key": key}).Info("key readed")

	val, err := c.ReadBytes('\n')
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Error("on reading value")
		return err
	}

	logrus.WithFields(logrus.Fields{"value": string(val)}).Info("value readed")

	ctx, cancel := context.WithTimeout(context.Background(), s.TTLSecond*time.Second)
	key = key[:len(key)-1]
	val = val[:len(val)-1]
	s.Storage.Set(key, val, ctx, cancel)
	c.WriteString("OK\n")
	return nil
}

func (s *Server) getCommand(c integral.Client) error {
	key, err := c.ReadString('\n')
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Error("on reading key")
		return err
	}

	key = key[:len(key)-1]
	val, err := s.Storage.Get(key)
	if err != nil {
		return err
	}

	if _, err := c.Write(append(val, '\n')); err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Error("on sending value")
		return err
	}

	return nil
}

func (s *Server) delCommand(c integral.Client) error {

	key, err := c.ReadString('\n')
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Error("on reading key")
		return err
	}

	key = key[:len(key)-1]
	s.Storage.Delete(key)

	return nil
}
