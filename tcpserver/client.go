package tcpserver

import (
	"bufio"
	"io"

	"bitbucket.org/losaped/integral"
	log "github.com/sirupsen/logrus"
)

// Client implement integral.Client interface
type Client struct {
	reader *bufio.Reader
	writer *bufio.Writer
}

func NewClient(r io.Reader, w io.Writer) Client {
	return Client{
		reader: bufio.NewReader(r),
		writer: bufio.NewWriter(w),
	}
}

func (c Client) ReadString(b byte) (string, error) {
	return c.reader.ReadString(b)
}

func (c Client) ReadBytes(b byte) ([]byte, error) {
	return c.reader.ReadBytes(b)
}

func (c Client) Write(buf []byte) (count int, err error) {
	count, err = c.writer.Write(buf)
	if err != nil {
		log.WithFields(log.Fields{"type": integral.IOError, "error": err}).Error("on write bytes")
		return
	}

	err = c.writer.Flush()
	if err != nil {
		log.WithFields(log.Fields{"type": integral.IOError, "error": err}).Error("on flush bytes")
	}
	return
}

func (c Client) WriteString(s string) (count int, err error) {
	count, err = c.writer.WriteString(s)
	if err != nil {
		log.WithFields(log.Fields{"type": integral.IOError, "error": err}).Error("on write string")
		return
	}

	err = c.writer.Flush()
	if err != nil {
		log.WithFields(log.Fields{"type": integral.IOError, "error": err}).Error("on flush string")
	}
	return
}
