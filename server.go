package integral

import (
	"context"
	"errors"
)

var ErrorUnknownCommand = errors.New("Command unknown")

type Server interface {
	Run(context.Context)
}
