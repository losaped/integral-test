package mapstorage

import (
	"sync"

	"context"

	"bitbucket.org/losaped/integral"
)

type Value struct {
	content []byte
	cancel  context.CancelFunc
}

// Storage implementation of integral storage based on map
type Storage struct {
	m       sync.RWMutex
	storage map[string]Value
}

func NewStorage() *Storage {
	return &Storage{
		storage: make(map[string]Value),
	}
}

func (s *Storage) Get(key string) ([]byte, error) {
	s.m.Lock()
	defer s.m.Unlock()

	val, exists := s.storage[key]
	if !exists {
		return nil, integral.ErrorKeyNotFound
	}

	val.cancel()
	return val.content, nil
}

func (s *Storage) Delete(key string) {
	s.m.RLock()
	defer s.m.RUnlock()

	if v, exists := s.storage[key]; exists {
		v.cancel()
	}
}

func (s *Storage) internalDelete(key string) {
	s.m.Lock()
	defer s.m.Unlock()

	delete(s.storage, key)
}

func (s *Storage) Set(key string, val []byte, ctx context.Context, cancel context.CancelFunc) {
	s.m.Lock()
	s.storage[key] = Value{content: val, cancel: cancel}
	s.m.Unlock()

	go func() {
		<-ctx.Done()
		s.internalDelete(key)
	}()
}

func (s *Storage) Stop() {
	for _, val := range s.storage {
		val.cancel()
	}
}
