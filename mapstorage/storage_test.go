package mapstorage

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/losaped/integral"
)

func TestSetWithTTL(t *testing.T) {
	s := NewStorage()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2*time.Second))
	s.Set("first", []byte("first value"), ctx, cancel)
	time.Sleep(time.Duration(3 * time.Second))
	val, err := s.Get("first")
	if err == nil {
		t.Errorf("must return error '%s'", integral.ErrorKeyNotFound)
		return
	}

	if val != nil {
		t.Errorf("value must be nil")
	}

	if err != integral.ErrorKeyNotFound {
		t.Errorf("must return error '%s'", integral.ErrorKeyNotFound)
	}
}

func TestSetValueWithExistingKey(t *testing.T) {
	s := NewStorage()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2*time.Second))
	s.Set("first", []byte("first value"), ctx, cancel)
	time.Sleep(time.Duration(3 * time.Second))

	secondContext, secondCancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	s.Set("first", []byte("changed first value"), secondContext, secondCancel)
	val, err := s.Get("first")
	if err != nil {
		t.Error(err)
		return
	}

	if string(val) != "changed first value" {
		t.Error("val mustbe a 'changed first value'")
	}
}
