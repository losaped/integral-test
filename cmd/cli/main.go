package main

import (
	"bufio"
	"fmt"
	"net"
	"time"

	"github.com/sirupsen/logrus"
)

func main() {
	con, err := net.Dial("tcp", "localhost:7878")
	if err != nil {
		logrus.Fatal(err)
	}

	reader := bufio.NewReader(con)
	writer := bufio.NewWriter(con)

	cmd := []byte("set\nfirstKey\nonetwothree\n") //

	n, err := writer.Write(cmd)
	if err != nil {
		logrus.Fatal(err)
	}
	writer.Flush()

	logrus.Info("writen", n)
	str, err := reader.ReadString('\n')
	if err != nil {
		logrus.Fatal(err)
	}
	fmt.Println(str)
	//====================================
	fmt.Println("sending get")
	cmd = []byte("get\nfirstKey\n")
	_, err = writer.Write(cmd)
	if err != nil {
		logrus.Fatal(err)
	}

	writer.Flush()

	str, err = reader.ReadString('\n')
	if err != nil {
		logrus.Fatal(err)
	}
	fmt.Println(str)
	//====================================
	cmd = []byte("get\nfirstKey\n")
	_, err = writer.Write(cmd)
	if err != nil {
		logrus.Fatal(err)
	}
	writer.Flush()

	str, err = reader.ReadString('\n')
	if err != nil {
		logrus.Fatal(err)
	}
	fmt.Println(str)

	//========================================
	cmd = []byte("set\nfirstKey\nonetwothree\n") //
	_, err = writer.Write(cmd)
	if err != nil {
		logrus.Fatal(err)
	}

	writer.Flush()

	str, err = reader.ReadString('\n')
	if err != nil {
		logrus.Fatal(err)
	}

	fmt.Println(str)

	time.Sleep(31 * time.Second)
	cmd = []byte("get\nfirstKey\n")
	_, err = writer.Write(cmd)
	if err != nil {
		logrus.Fatal(err)
	}

	writer.Flush()

	str, err = reader.ReadString('\n')
	if err != nil {
		logrus.Fatal(err)
	}

	fmt.Println(str)

}
