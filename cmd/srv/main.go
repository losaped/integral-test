package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/losaped/integral/mapstorage"
	"bitbucket.org/losaped/integral/tcpserver"
	"github.com/sirupsen/logrus"
)

func main() {
	conf := &tcpserver.Config{
		Port:      7878,
		TTLSecond: time.Duration(1),
	}

	server := tcpserver.Server{
		Config:  conf,
		Storage: mapstorage.NewStorage(),
	}

	ctx, cancel := context.WithCancel(context.Background())
	go waitSignal(cancel)
	logrus.Error(server.Run(ctx))
}

func waitSignal(cancel context.CancelFunc) {
	sigChan := make(chan os.Signal, 1)
	defer close(sigChan)

	signal.Notify(sigChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	<-sigChan
	signal.Stop(sigChan)

	cancel()
}
